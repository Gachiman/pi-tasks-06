#pragma once
#include "Group.h"
#include "Student.h"
#include <string>
using namespace std;

class Dekanat
{
private:
	Group **groups;
	Student **studMassiv;
	int groupsNum;
	int studNum;
public:
	Dekanat();
	~Dekanat();
	void addGroup(Group *gr);
	void addStudent(Student *stud);
	void loadGroups();
	void loadStudents();
	Student* findStudent(int id);
	Group* findGroup(string name);
	void distributeStudents();
	void randomMarks();
	void replaceStudent(Student *st, Group *destination);
	void groupsAvgMarks();
	void StudentsAvgMarks();
	void showGroups();
	void showStudents();
	void showHonours();
	void randomGroupsHeads();
	void sendDown();
	void saveNewStudentsList();
	void saveNewGroupsList();
};

