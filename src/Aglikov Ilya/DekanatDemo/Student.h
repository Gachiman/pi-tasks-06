#pragma once
#include <string>
#include "Group.h"
using namespace std;

class Group;

class Student
{
private: int ID, numMarks;
		 string Fio;
		 Group *gr;
		 int *marks;
public:
	Student();
	Student(int _ID, string _Fio);
	Student(const Student &st);
	~Student();

	void setID(int _ID);
	void setFio(string _Fio);
	void setGroup(Group *_gr);

	int getID();
	string getFio();
	Group* getGroup();
	
	void addMark(int _mark);
	double getAvgMark();
	void showMarks(ostream& stream);
	
};

