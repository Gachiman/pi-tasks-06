#include "stdafx.h"
#include "Group.h"
#include <string>
#include <ctime>
#include <iostream>


Group::Group() : title(" "), studNum(0), studMassiv(nullptr), head(nullptr) {};
Group::Group(string _title) : title(_title), studNum(0), studMassiv(nullptr), head(nullptr) {};
Group::Group(const Group &gr)
{
	title = gr.title;
	studNum = gr.studNum;
	if (studNum)
	{
		studMassiv = new Student*[studNum];
		for (int i = 0; i < studNum; i++)
			studMassiv[i] = gr.studMassiv[i];
		head = gr.head;
	}
}

Group::~Group()
{
	delete[] studMassiv;
	studMassiv = nullptr;
}

void Group::setTitle(string _title) { title = _title; }
string Group::getTitle() { return title; }

void Group::addStudent(Student *_stud)
{
	if (!studNum)
	{
		studMassiv = new Student*[1];
		studMassiv[studNum] = _stud;
	}
	else
	{
		Student **temp = new Student*[studNum + 1];
		for (int i = 0; i < studNum; i++)
			temp[i] = studMassiv[i];
		delete[] studMassiv;
		studMassiv = temp;
		studMassiv[studNum] = _stud;
	}
	_stud->setGroup(this);
	studNum++;
}

Student* Group::findStudent(int _id)
{
	int i = 0;
	if (!studNum)
	{
		return nullptr;
	}
	else
	{
		for (i = 0; i < studNum; i++)
			if (studMassiv[i]->getID() == _id)
				return studMassiv[i];
		if (i == studNum)
		{
			return nullptr;
		}
	}
}

void Group::removeStudent(int _id)
{
	int i = 0;
	for (i; i < studNum; i++)
		if (studMassiv[i]->getID() == _id)
		{
			if (head == studMassiv[i])
				head = nullptr;
			if (studMassiv[i] != studMassiv[studNum - 1])
				studMassiv[i] = studMassiv[studNum - 1];
			studMassiv[studNum - 1] = nullptr;
			studNum--;
			break;
		}
}

void Group::setHead(int _id)
{
	head = findStudent(_id);
}

void Group::randomHead()
{
	srand((int)time(NULL));
	if (studNum)
		head = studMassiv[rand() % studNum];
}

Student* Group::getHead()
{
	if (!head)
	{
		cout << "The headman hasn't been chosen" << endl;
		return nullptr;
	}
	else
		return head;
}

double Group::getAvgMark()
{
	double avgMark = 0.0;
	if (!studNum)
	{
		cout << "There are no students in this group  (" << title << ")" << endl;
		return 0;
	}
	else
	{
		for (int i = 0; i < studNum; i++)
			avgMark += studMassiv[i]->getAvgMark();
		avgMark /= studNum;
		return avgMark;
	}
}

void Group::findHonoursPupils()
{
	if (!studNum)
	{
		cout << "There are no students in this group (" << title << ")" << endl;
		return;
	}
	else
	{
		bool isExist = false;
		cout << "\nHonours pupils:" << endl;
		for (int i = 0; i < studNum; i++)
			if (studMassiv[i]->getAvgMark() == 5)
			{
				isExist = true;
				cout << studMassiv[i]->getID() << " " << studMassiv[i]->getFio() << endl;
			}
		if (!isExist)
			cout << "there are no honours pupils in this group (" << title << ")" << endl;
	}
}

void Group::showGroupInfo(ostream& stream)
{
	stream << "\n###################################\n" << title << "\n" << endl;
	if (!studNum)
	{
		stream << "There are no students in this group" << endl;
		stream << "###################################" << endl;
		return;
	}
	else
	{
		stream << "Students:" << endl;
		for (int i = 0; i < studNum; i++)
		{
			stream << "--------------------------------------\n" << studMassiv[i]->getID() << " ";
			stream << studMassiv[i]->getFio() << "\n" << "Marks: ";
			studMassiv[i]->showMarks(stream);
			stream << "Average mark: " << studMassiv[i]->getAvgMark() << endl;
			stream << "--------------------------------------" << endl;
		}
		stream << "Headman:" << endl;
		if (!head)
			stream << "The headman hasn't been chosen" << endl;
		else
			stream << getHead()->getID() << " " << getHead()->getFio() << endl;
		stream << "--------------------------------------\n" << "Average mark in the group: ";
		stream << getAvgMark() << "\n--------------------------------------" << endl;
		stream << "###################################" << endl;
	}
}