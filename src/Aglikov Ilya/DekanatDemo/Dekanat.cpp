#include "stdafx.h"
#include "Dekanat.h"
#include <string>
#include <fstream>
#include <ctime>
#include <iostream>
#define RANDOM_MARKS_NUMBER 10

Dekanat::Dekanat() : groups(nullptr), studMassiv(nullptr), groupsNum(0), studNum(0) {};

Dekanat::~Dekanat()
{
	for (int i = 0; i < groupsNum; i++)
		delete groups[i];
	delete[] groups;
	groups = nullptr;
	for (int i = 0; i < studNum; i++)
		delete studMassiv[i];
	delete[] studMassiv;
	studMassiv = nullptr;
}

void Dekanat::addGroup(Group *gr)
{
	if (!groupsNum)
	{
		groups = new Group*[1];
		groups[groupsNum] = gr;
	}
	else
	{
		Group **temp = new Group*[groupsNum + 1];
		for (int i = 0; i < groupsNum; i++)
			temp[i] = groups[i];
		delete[] groups;
		groups = temp;
		groups[groupsNum] = gr;
	}
	groupsNum++;
}

void Dekanat::addStudent(Student *stud)
{
	if (!studNum)
	{
		studMassiv = new Student*[1];
		studMassiv[studNum] = stud;
	}
	else
	{
		Student **temp = new Student*[studNum + 1];
		for (int i = 0; i < studNum; i++)
			temp[i] = studMassiv[i];
		delete[] studMassiv;
		studMassiv = temp;
		studMassiv[studNum] = stud;
	}
	studNum++;
}

void Dekanat::loadGroups()
{
	ifstream groupsFile("..\\resources\\groups.txt");
	if (!groupsFile.is_open())
	{
		cout << "File \"groups.txt\" can not be opened" << endl;
		exit(EXIT_FAILURE);
	}
	while (!groupsFile.eof())
	{
		string currentGroup = "";
		getline(groupsFile, currentGroup);
		addGroup(new Group(currentGroup));
	}
	groupsFile.close();
}

void Dekanat::loadStudents()
{
	ifstream studentsFile("..\\resources\\students.txt");
	if (!studentsFile.is_open())
	{
		cout << "File \"students.txt\" can not be opened" << endl;
		exit(EXIT_FAILURE);
	}
	while (!studentsFile.eof())
	{
		string currentID = "";
		string currentName = "";
		getline(studentsFile, currentID);
		getline(studentsFile, currentName);
		addStudent(new Student(atoi(currentID.c_str()), currentName));
	}
	studentsFile.close();
}

Student* Dekanat::findStudent(int id)
{
	int i = 0;
	if (!studNum)
	{
		cout << "There are no students in the University" << endl;
		return nullptr;
	}
	else
	{
		for (i = 0; i < studNum; i++)
			if (studMassiv[i]->getID() == id)
				return studMassiv[i];
		if (i == studNum)
		{
			cout << "There is no student with this ID in the University" << endl;
			return nullptr;
		}
	}
}

Group* Dekanat::findGroup(string name)
{
	int i = 0;
	if (!groupsNum)
	{
		cout << "There are no groups in the University" << endl;
		return nullptr;
	}
	else
	{
		for (i = 0; i < groupsNum; i++)
			if (strcmp(groups[i]->getTitle().c_str(), name.c_str()) == 0)
				return groups[i];
		if (i == groupsNum)
		{
			cout << "There is no group with this title in the University" << endl;
			return nullptr;
		}
	}
}

void Dekanat::distributeStudents()
{
	int curGr;
	int maxGroupIndex = groupsNum - 1;
	srand(time(0));
	for (int i = 0; i < studNum; i++)
		if (!studMassiv[i]->getGroup())
		{
			curGr = rand() % (maxGroupIndex + 1);
			groups[curGr]->addStudent(studMassiv[i]);
		}
}

void Dekanat::randomMarks()
{
	for (int i = 0; i < studNum; i++)
		for (int j = 0; j < RANDOM_MARKS_NUMBER; j++)
			studMassiv[i]->addMark(rand() % 5 + 1);
}

void Dekanat::replaceStudent(Student *st, Group *destination)
{
	if (!st || !destination)
	{
		cout << "Incorrect data! Try again" << endl;
		return;
	}
	if (!(st->getGroup()))
		return;
	st->getGroup()->removeStudent(st->getID());
	destination->addStudent(st);
}

void Dekanat::groupsAvgMarks()
{
	double maxAvgMark = 0.;
	Group *tmp = nullptr;
	cout << "\n###################################\nGroup average mark:\n" << endl;
	for (int i = 0; i < groupsNum; i++)
	{
		cout << groups[i]->getTitle() << ": ";
		cout << groups[i]->getAvgMark() << endl;
		if (groups[i]->getAvgMark() > maxAvgMark)
		{
			maxAvgMark = groups[i]->getAvgMark();
			tmp = groups[i];
		}
	}
	cout << "\nBest group:\n" << endl;
	cout << tmp->getTitle() << endl << "Average Mark: ";
	cout << tmp->getAvgMark() << endl << "\n###################################\n" << endl;

}

void Dekanat::StudentsAvgMarks()
{
	double maxAvgMark = 0.;
	Student *tmp = nullptr;
	cout << "\n###################################\nStudent average mark:\n" << endl;
	for (int i = 0; i < studNum; i++)
	{
		cout << studMassiv[i]->getID() << " " << studMassiv[i]->getFio() << ": " << studMassiv[i]->getAvgMark() << endl;
		if (studMassiv[i]->getAvgMark() > maxAvgMark)
		{
			maxAvgMark = studMassiv[i]->getAvgMark();
			tmp = studMassiv[i];
		}
	}
	cout << "\nBest student:\n" << endl;
	cout << tmp->getFio() << endl << "Average Mark: ";
	cout << tmp->getAvgMark() << endl << "\n###################################\n" << endl;

}

void Dekanat::showGroups()
{
	if (!groupsNum)
	{
		cout << "There are no groups in the University" << endl;
		return;
	}
	else
	{
		cout << "\nUniversity Groups:" << endl;
		for (int i = 0; i < groupsNum; i++)
			groups[i]->showGroupInfo(cout);
	}
}

void Dekanat::showStudents()
{
	if (!studNum)
	{
		cout << "There are no students in the University" << endl;
		return;
	}
	else
	{
		cout << "\nUNIVERSITY STUDENTS:" << endl;
		for (int i = 0; i < studNum; i++)
			cout << studMassiv[i]->getID() << " " << studMassiv[i]->getFio() << endl;
	}
}

void Dekanat::showHonours()
{
	for (int i = 0; i < groupsNum; i++)
		groups[i]->findHonoursPupils();
}

void Dekanat::randomGroupsHeads()
{
	for (int i = 0; i < groupsNum; i++)
		groups[i]->randomHead();
}

void Dekanat::sendDown()
{
	int curID = 0;
	Student **tmp = nullptr;
	int count = studNum;
	for (int i = 0; i < studNum; i++)
		if (studMassiv[i]->getAvgMark() < 3.0)
		{
			curID = studMassiv[i]->getID();
			studMassiv[i]->getGroup()->removeStudent(curID);
			delete studMassiv[i];
			studMassiv[i] = nullptr;
			count--;
		}
	tmp = new Student*[count];
	count = 0;
	for (int i = 0; i < studNum; i++)
		if (studMassiv[i])
		{
			tmp[count] = studMassiv[i];
			count++;
		}
	delete[] studMassiv;
	studMassiv = tmp;
	studNum = count;
}

void Dekanat::saveNewStudentsList()
{
	ofstream stFile("..\\resources\\new_students.txt");
	if (!stFile.is_open())
	{
		cout << "Error while creating file" << endl;
		exit(EXIT_FAILURE);
	}
	stFile << "UNIVERSITY STUDENTS:\n" << endl;
	for (int i = 0; i < studNum; i++)
		stFile << studMassiv[i]->getID() << " " << studMassiv[i]->getFio() << endl;
	stFile.close();
}

void Dekanat::saveNewGroupsList()
{
	ofstream grFile("..\\resources\\new_groups.txt");
	if (!grFile.is_open())
	{
		cout << "Error while creating file" << endl;
		exit(EXIT_FAILURE);
	}
	grFile << "UNIVERSITY GROUPS:\n" << endl;
	for (int i = 0; i < groupsNum; i++)
		groups[i]->showGroupInfo(grFile);
	grFile.close();
}
