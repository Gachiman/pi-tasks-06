#include "stdafx.h"
#include "Group.h"
#include "Student.h"
#include "Dekanat.h"
#include <iostream>

int main()
{
	Dekanat *dekan = new Dekanat();	//�������� ������� ������ "�������"
	dekan->loadGroups();	// �������� ������ ����� �� �����.
	dekan->loadStudents();	// �������� ������ ��������� �� �����.
	dekan->distributeStudents();	// ��������� ������������� ��������� �� �������.
	dekan->randomMarks();	// ����������� 10 �������� ������ ������� ��������.
	dekan->StudentsAvgMarks(); // ����� �� ������� ���������� � ������� ������������ ���������. 
	dekan->sendDown();	// ���������� �������� �� ��������������.
	dekan->showStudents();	// ����� �� ������� ������ ������������� ���������.
	dekan->randomGroupsHeads();	// ���������� ��������� ������� � ������.
	dekan->showGroups();	// ����� �� ������� ������� �������������� �����.
	dekan->groupsAvgMarks();	// ������������ �����. ����� ���������� � ������ ������.
	dekan->saveNewStudentsList();	// ���������� ������������� ��������� � ����.
	dekan->saveNewGroupsList();		// ���������� ���������� � ������� � ����.
									
	// ������ ������ "������� �������� � ������ ������".
	Group *gr = new Group("000000-0 Custom group");	// �������� ����� ������.
	Student *st = new Student(0, "Custom student");	// �������� ������ ��������.
	dekan->addGroup(gr);	// ���������� ��������� ������ � ����� ������ �����.
	dekan->addStudent(st); // ���������� ���������� �������� � ����� ������ ���������.
	dekan->findGroup("000000-0 Custom group")->addStudent(dekan->findStudent(0)); // ���������� ���������� �������� 
																				  // � ��������� ������.

	dekan->findGroup("000000-0 Custom group")->showGroupInfo(cout); // ����� ���������� � ��������� ������.
	dekan->replaceStudent(dekan->findStudent(0), dekan->findGroup("381608-1 SWE")); // ������� ���������� �������� � ������ ������.

	// ��������.
	dekan->findGroup("000000-0 Custom group")->showGroupInfo(cout);
	dekan->findGroup("381608-1 SWE")->showGroupInfo(cout);
	system("pause");
    return 0;
}

