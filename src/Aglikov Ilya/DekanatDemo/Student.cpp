#include "stdafx.h"
#include "Student.h"
#include "Group.h"

Student::Student() : ID(0), Fio(""), marks(nullptr), gr(nullptr), numMarks(0) {};

Student::Student(int _ID, string _Fio) : Fio(_Fio), ID(_ID), gr(nullptr), marks(nullptr), numMarks(0) {};

Student::Student(const Student &st)
{
	ID = st.ID;
	Fio = st.Fio;
	gr = st.gr;
	numMarks = st.numMarks;
	if (numMarks)
	{
		marks = new int[numMarks];
		for (int i = 0; i < numMarks; i++)
			marks[i] = st.marks[i];
	}
}

Student::~Student()
{
	delete[] marks;
	marks = nullptr;
}

void Student::setID(int _ID) { ID = _ID; }
void Student::setFio(string _Fio) { Fio = _Fio; }
void Student::setGroup(Group *_gr) { gr = _gr; }
int Student::getID() { return ID; }
string Student::getFio() { return Fio; }
Group* Student::getGroup() { return gr; }

void Student::addMark(int _mark)
{
	if (!numMarks)
	{
		marks = new int[1];
		marks[numMarks++] = _mark;
	}
	else
	{
		int *temp = new int[numMarks + 1];
		for (int i = 0; i < numMarks; i++)
			temp[i] = marks[i];
		temp[numMarks++] = _mark;
		delete[] marks;
		marks = temp;
	}
}

double Student::getAvgMark()
{
	double avgMark = 0.0;
	if (!numMarks)
		return 0;
	else
	{
		for (int i = 0; i < numMarks; i++)
			avgMark += marks[i];
		avgMark /= numMarks;
		return avgMark;
	}
}

void Student::showMarks(ostream& stream)
{
	if (!numMarks)
	{
		stream << "This student has got no marks" << endl;
		return;
	}
	else
	{
		for (int i = 0; i < numMarks; i++)
			stream << marks[i] << " ";
		stream << endl;
	}
}
