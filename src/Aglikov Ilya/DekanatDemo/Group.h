#pragma once
#include <string>
#include "Student.h"
using namespace std;
class Student;

class Group
{
private:
	string title;
	int studNum;
	Student **studMassiv;
	Student *head;
public:
	Group();
	explicit Group(string _title);
	Group(const Group &_gr);
	~Group();
	void setTitle(string _title);
	string getTitle();
	void addStudent(Student *_stud);
	Student* findStudent(int _id);
	void removeStudent(int _id);
	void setHead(int _id);
	void randomHead();
	Student* getHead();
	double getAvgMark();
	void findHonoursPupils();
	void showGroupInfo(ostream& stream);
};

